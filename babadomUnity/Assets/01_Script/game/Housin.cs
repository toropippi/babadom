﻿using UnityEngine;
using System.Collections;

public class Housin : MonoBehaviour
{

	// プレハブ
	GameObject _prefab = null;
	//大玉がでるX座標
	public float HousinX = 0.0f;
	//大玉がでるY座標
	float HousinY = 2.142014f;
	//砲身アニメカウンター
	int cnt = 0;
	//砲身の初期位置記憶用
	Vector2 housin_Syoki_xy;
	//砲身のridibody使う
	Rigidbody2D housinRigi;
	//重力加速度
	float G = -0.0308f;

	// Use this for initialization
	void Start()
	{
		//砲身のridibody使う
		housinRigi = GetComponent<Rigidbody2D>();
		housin_Syoki_xy = housinRigi.position;
	}

	// Update is called once per frame
	void Update()
	{
		//撃った後に砲身が引くアニメーション
		if (cnt != 0)
		{
			float revflg = -1.0f;
			if (HousinX > 0.0f)
			{
				revflg = 1.0f;
			}
			//砲身のridibody使う
			housinRigi.position = housin_Syoki_xy + cnt * (new Vector2(0.002f * revflg, -0.002f));
			cnt--;
		}
		//砲身が撃った弾が画面上に存在しているかどうか
		if (this.transform.childCount < GameManagerGod.level)
		{
			if (Random.Range(0, Mathf.Clamp(3 * (104 - GameManagerGod.level * GameManagerGod.level), 1, 999999)) == 0)
			{
				//大砲生成
				Add(new Vector3(HousinX, HousinY, 2.0f));
				cnt = 12;
			}
		}
	}

	// 大弾の生成,大弾を子オブジェクトとして生成
	GameObject Add(Vector3 xyz)
	{
		// プレハブを取得
		_prefab = Resources.Load("tama_big") as GameObject;
		//大玉のインスタンス生成＝画面上に出す
		GameObject g = Instantiate(_prefab, xyz, Quaternion.identity) as GameObject;
		//親子関係セット
		g.transform.parent = this.transform;
		//弾の弾道を計算
		CalcTamaBig(g);
		return g;
	}

	//弾の弾道を計算
	void CalcTamaBig(GameObject g)
	{
		//まずUFoのポジション取得
		Vector2 ufopos = UfoMouseMove.GetUfoPos();
		//弾の速度、重力加速度を設定したい
		TamaBig tmBgData = g.GetComponent<TamaBig>();
		//弾のスピード等をセット
		float calc_xu = ufopos.x - HousinX;
		float calc_yu = ufopos.y - HousinY;
		float calc_a;
		if (Mathf.Abs(calc_xu) < 0.0005f)
		{
			calc_xu = (GameManagerGod.riff(calc_xu >= 0.0f) * 2.0f - 1.0f) * 0.0005f;
		}
		calc_a = (0.5f * calc_xu * (GameManagerGod.riff(HousinX > 0.0) * 2.0f - 1.0f) + calc_yu) / (calc_xu * calc_xu);
		tmBgData.G = G / GameManagerGod.flmRate;
		float calc_tmp = 0.5f * tmBgData.G / calc_a;
		calc_tmp = Mathf.Clamp(calc_tmp, 0.000001f, 999999.9f);
		tmBgData.spdx = -Mathf.Sqrt(calc_tmp) * (GameManagerGod.riff(HousinX > 0.0) * 2.0f - 1.0f);
		tmBgData.spdy = 0.5f * Mathf.Abs(tmBgData.spdx);
		//大玉のxスピードが決定された
	}
	/*
	//条件式を整数で返す
	int rif(bool p1)
	{
		if (p1 == true)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	//条件式を浮動小数点で返す
	float riff(bool p1)
	{
		if (p1 == true)
		{
			return 1.0f;
		}
		else
		{
			return 0.0f;
		}
	}
	*/
}