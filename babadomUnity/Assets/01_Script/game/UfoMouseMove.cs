﻿using UnityEngine;
using System.Collections;

public class UfoMouseMove : MonoBehaviour
{
	/*
    //アニメーションセッティンつ
    Animation _anim;
    */
	// UFOの基本スピード
	public float ufo_speed = 1.61f;
	//係数
	float speed_event;
	// UFOがマウスの周りを回るときの半径
	public float ufo_radius = 0.08f;
	//目標地点は UFO→マウス ベクトルに対してomwga/360回転分左に回転した位置.
	public float omwga = 0.60f;
	// ワールド座標に変換されたマウス座標を代入
	Vector3 screenToWorldPointPosition;
	//マウス座標とUFO座標の差
	Vector2 MouseUfoPosSub;
	//マウス座標とUFO座標の差の角度
	float M_U_rad;
	//反動等による慣性
	public Vector2 inertia;
	//ufoのridibody使う
	Rigidbody2D ufoRigi;

	void Start()
	{
		//ufoのridibody使う
		ufoRigi = GetComponent<Rigidbody2D>();
		//初期位置
		ufoRigi.position = new Vector2(0.0f, 1.2f);
		//慣性
		inertia = new Vector2(0.0f, 0.0f);
	}

	// Update is called once per frame
	void Update()
	{
		//マウスのワールド座標取得
		screenToWorldPointPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		//マウス座標とUFO座標の差
		MouseUfoPosSub.x = screenToWorldPointPosition.x - ufoRigi.position.x;
		MouseUfoPosSub.y = screenToWorldPointPosition.y - ufoRigi.position.y;
		M_U_rad = Mathf.Atan2(MouseUfoPosSub.y, MouseUfoPosSub.x);

		//目標地点は マウス→UFO ベクトルに対してomwga/360回転分右に回転した位置.
		M_U_rad += omwga - 3.1415926535f;
		MouseUfoPosSub.x += Mathf.Cos(M_U_rad) * ufo_radius;
		MouseUfoPosSub.y += Mathf.Sin(M_U_rad) * ufo_radius;
		//単位ベクトルをMouseUfoPosSubに入れる
		MouseUfoPosSub.Normalize();
		//オブジェクトの速度にセット
		//ufoRigi.velocity = MouseUfoPosSub * ufo_speed;

		//移動(rigi,vector2 速度)
		speed_event = 1.0f;
		if (UfoDust.MouseDownNow == 1) speed_event = 0.4f;
		Velocity(ufoRigi, MouseUfoPosSub * ufo_speed * speed_event + inertia);

		//慣性の計算
		//まず速度の1乗に比例して減速
		float inertia_len = inertia.magnitude;
		float inertia_sub_speed = 0.18f * inertia_len * +1.0f;
		if (inertia_len < 0.17f)
		{
			inertia *= 0f;
		}
		else
		{
			inertia -= 0.17f * inertia_sub_speed * inertia.normalized;
		}
		//次に境界条件
		if (ufoRigi.position.x > 2.2)
		{
			//反射
			if (inertia.x > 0.0f) inertia.x = -0.88f * inertia.x;
			inertia.x -= 8.0f;//さらに反発成分も加わる
		}
		if (ufoRigi.position.x < -2.2)
		{
			//反射
			if (inertia.x < 0.0f) inertia.x = -0.88f * inertia.x;
			inertia.x += 8.0f;//さらに反発成分も加わる
		}

		if (ufoRigi.position.y < -1.51)
		{
			//反射
			if (inertia.y < 0.0f) inertia.y = -0.88f * inertia.y;
			inertia.y += 6.1f;
		}
		if (ufoRigi.position.y > 1.81)
		{
			//反射
			if (inertia.y > 0.0f) inertia.y = -0.88f * inertia.y;
			inertia.y -= 1.1f;
		}
	}

	//自作移動ルーチン
	void Velocity(Rigidbody2D _rigi, Vector2 speed)
	{
		_rigi.transform.position = _rigi.position + speed / GameManagerGod.flmRate;
	}

	//どのクラスからも使えるように
	//Ufoの座標を返す関数
	public static Vector2 GetUfoPos()
	{
		//UFOのオブジェを取得
		GameObject _gameobj = GameObject.Find("ufo");
		Rigidbody2D _ufo = _gameobj.GetComponent<Rigidbody2D>();
		return _ufo.position;
	}

	//どのクラスからも使えるように
	//Ufoに外力を加える関数
	public static void AddFUfo(Vector2 normalizedF, float scale)
	{
		//UFOのオブジェを取得
		GameObject _gameobj = GameObject.Find("ufo");
		UfoMouseMove _ufo = _gameobj.GetComponent<UfoMouseMove>();
		//慣性に外力を加える
		_ufo.inertia += normalizedF * scale;
	}
}