﻿using UnityEngine;
using System.Collections;

public class Syouidan : MonoBehaviour
{
	Rigidbody2D syouidanRigi;
	// Use this for initialization
	int flm;
	void Start()
	{
		syouidanRigi = GetComponent<Rigidbody2D>();
		flm = 1;
		//syouidanRigi.velocity = new Vector2(0.0f, -0.5f);
		syouidanRigi.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
	}

	// Update is called once per frame
	void Update()
	{
		//画面当たり判定
		if (syouidanRigi.position.y < -1.7)
		{
			Destroy(gameObject);
		}
		//移動
		Velocity(syouidanRigi);

		//うまれた瞬間から4フレーム目まで大きさかえる
		if (flm < 5) { syouidanRigi.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f) * flm; }
		flm++;

	}

	//自作移動ルーチン
	void Velocity(Rigidbody2D _rigi)
	{
		_rigi.transform.position = _rigi.position - new Vector2(0.0f, 0.5f / GameManagerGod.flmRate);
	}


	//衝突判定
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			//Rigidbody2D 自分のrigi;
			Rigidbody2D my_rigi = this.GetComponent<Rigidbody2D>();
			Vector2 colPos = my_rigi.position;
			Rigidbody2D u_rigi = col.GetComponent<Rigidbody2D>();
			Vector2 ufoPos = u_rigi.position;
			//Vector2 ufoPos = UfoMouseMove.GetUfoPos();
			//長さ1ベクトル計算
			Vector2 fv = ufoPos - colPos;
			fv.Normalize();
			//ufoに外力加える
			UfoMouseMove.AddFUfo(fv, 6.7f);
			//HP操作
			UfoGage.DmgdHp(0.2f);
			//自分は消滅
			Destroy(gameObject);
		}
	}


}
