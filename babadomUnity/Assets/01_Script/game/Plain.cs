﻿using UnityEngine;
using System.Collections;

public class Plain : MonoBehaviour
{
	//ソースのコンポーネント
	PlainParent parentObj_PrainParent;
	//リギのコンポーネント
	Rigidbody2D plainRigi;
	// プレハブ
	GameObject _prefab;
	//焼夷弾を撃つタイミング等のフレームを管理
	int localFlm;
	//焼夷弾を撃つフレームを配列で管理。0なら打たない1なら撃つフレーム
	int[] shotFlmArray = new int[72];

	// Use this for initialization
	void Start()
	{
		//飛行機のリギ取得
		plainRigi = GetComponent<Rigidbody2D>();
		//親のソースコンポーネント取得
		parentObj_PrainParent = GetParentSourceComponent();
		parentObj_PrainParent.plainCount++;
		//速度と位置セット
		//plainRigi.velocity = new Vector2(0.0f, 0.0f);
		//plainRigi.position = new Vector2(3.9f, 2.07f);
		//自分が打つ予定の焼夷弾の数を計算して予めいつ撃つか決めておく
		ShotPlan(shotFlmArray);
		localFlm = 0;
	}

	// Update is called once per frame
	void Update()
	{
		//画面左に行ったら消滅
		if (plainRigi.position.x < -3.8)
		{
			parentObj_PrainParent.plainCount--;//制御している親のクラスの記憶している飛行機の数を1へらす
			Destroy(gameObject);
		}

		//焼夷弾がでるフレームが1のとき出す。
		if ((localFlm >= 25) & (localFlm < 25 + 72))
		{
			if (shotFlmArray[localFlm - 25] == 1)
			{
				//焼夷弾生成//生成位置は飛行機より少しした
				Add(plainRigi.position + new Vector2(0.0f, -0.2f));
			}
		}

		//移動
		Velocity(plainRigi);
		//フレーム加算
		localFlm++;
	}

	// 焼夷弾の生成,焼夷弾をPlainParentの子のオブジェクトとして生成
	GameObject Add(Vector3 xyz)
	{
		// プレハブを取得
		_prefab = Resources.Load("syouidan") as GameObject;
		//焼夷弾のインスタンス生成＝画面上に出す
		GameObject g = Instantiate(_prefab, xyz, Quaternion.identity) as GameObject;
		//親子関係セット
		g.transform.parent = this.transform.parent;
		return g;
	}

	//親が持っているソース（コンポーネント）を返す
	PlainParent GetParentSourceComponent()
	{
		//親のオブジェクト取得
		GameObject parentObj = this.transform.parent.gameObject;
		//飛行機の親のPrainParentを取得
		return parentObj.GetComponent<PlainParent>();
	}

	//自分が打つ予定の焼夷弾の数を計算して予めいつ撃つか決めておく
	void ShotPlan(int[] shotFlmArray)
	{
		//レベルによって１回で撃つ焼夷弾の数が異なる
		int myShotedCount = Mathf.Max(1, (GameManagerGod.level + Random.Range(0, 4) - 4) / 2);
		myShotedCount = Mathf.Min(72, myShotedCount);
		for (int i = 0; i < myShotedCount; i++)
		{
			//どのフレームにするかはランダム。ランダムで被らないようにランダム範囲を徐々に減らす
			//全部のフレームに1が入る場合の計算量はO(n^2)
			int setFlmNo = (int)Random.Range(0, 72 - i);
			//最初から数えてsetFlmNo番目の0を1にしたい
			int count = setFlmNo + 1;
			for (int j = 0; j < 72; j++)
			{
				if (shotFlmArray[j] == 0) { count--; }
				if (count == 0)
				{
					shotFlmArray[j] = 1;
					break;
				}
			}
		}
	}

	//自作移動ルーチン
	void Velocity(Rigidbody2D _rigi)
	{
		_rigi.transform.position = _rigi.position - new Vector2(3.9f / GameManagerGod.flmRate, 0.0f);
	}
}