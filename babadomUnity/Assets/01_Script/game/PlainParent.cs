﻿using UnityEngine;
using System.Collections;

public class PlainParent : MonoBehaviour
{
	//プレハブ初期化
	GameObject _prefab;
	//飛行機が出現するまでのフレームを管理
	int localFlm = 0;
	//飛行機が何機でているか。普通０か１機
	public int plainCount;

	void Start()
	{
		plainCount = 0;
	}

	// Update is called once per frame
	void Update()
	{
		//飛行機が0機の時に飛行機出す
		if (plainCount == 0)
		{
			//レベル0,1,2までは飛行機出さない
			if (GameManagerGod.level >= 3)
			{
				plainAddAlgorism();

            }
		}

		//ゲーム開始後フレーム加算
		localFlm++;
	}

	// PlainをPlainParentの子のオブジェクトとして生成
	GameObject Add()
	{
		// プレハブを取得
		_prefab = Resources.Load("Plain") as GameObject;
		//焼夷弾のインスタンス生成＝画面上に出す
		GameObject g = Instantiate(_prefab, new Vector3(3.9f, 2.12f, 0.0f), Quaternion.identity) as GameObject;
		//親子関係セット
		g.transform.parent = this.transform;
		return g;
	}

	void plainAddAlgorism()
	{
		//出現頻度
		if (localFlm > 500 + (10 - GameManagerGod.level) * 70)
		{
			//飛行機出現
			Add();
			//フレーム０にリセット
			localFlm = 0;
		}
	}

}
