﻿using UnityEngine;
using System.Collections;

public class Babadom : MonoBehaviour
{
	//初期化
	Rigidbody2D babadomRigi;
	public Vector2 speed;
	public float G;
	public int layer;
    public float power;//吸引力
	public int HP=32;//体力。<0で消滅
	//効果音のやつ
	private AudioSource[] sources;


	// Use this for initialization
	void Start()
	{
		//babadomのridibody使う
		babadomRigi = GetComponent<Rigidbody2D>();
		//効果音
		sources = gameObject.GetComponents<AudioSource>();
	}

	// Update is called once per frame
	void Update()
	{
		//角度0にもどす
		//transform.rotation = Quaternion.identity;
		//そこから何度回転させる
		//transform.Rotate(new Vector3(0.0f, 0.0f, 1.0f * Random.Range(-12, 12)));

		//吸引アルゴリズムで速度決定,スケール変換
		Move();
		//ババドン星人のy座標当たり判定は-1.49f
		//当たり判定
		PosClamp();
		//空気抵抗,速度リミット,体力
		AirResistance();
		//移動
		Velocity();
		//重力加速度計算
		Acceleration();
		if (HP==31) sources[0].Play();
		if (HP < 0)
		{
			Destroy(gameObject);
		}
	}


	//自作移動ルーチン
	void Velocity()
	{
		babadomRigi.transform.position = babadomRigi.position + speed / GameManagerGod.flmRate;
	}
	//空気抵抗
	void AirResistance()
	{
		//体力
		if (Mathf.Abs(speed.x) > 9.0f) HP--;
		if (Mathf.Abs(speed.y) > 9.0f) HP--;
		//速度リミット
		float tmpx = speed.x * speed.x;
		float tmpy = speed.y * speed.y;
		speed.x = speed.x / (1.0f + 0.002f * tmpx);
		speed.y = speed.y / (1.0f + 0.002f * tmpy);
		speed.x *= 0.9972f;
		speed.y *= 0.9972f;
		speed.x = Mathf.Clamp(speed.x, -5.8f, 5.8f);
		speed.y = Mathf.Clamp(speed.y, -5.8f, 5.8f);
	}
	//自作重力加速度計算
	void Acceleration()
	{
		speed.y += G / GameManagerGod.flmRate;
	}

	//ランダムで左右に速度がつく
	void RndWalk()
	{
		if (Random.Range(0, 27) == 0)
		{
			speed.x = 0.001f * Random.Range(-299, 299);
		}
	}

	//当たり判定
	void PosClamp()
	{
		//y座標、地面にめり込まないように
		if (babadomRigi.position.y <= -1.49f - 0.03f * layer)
		{
			babadomRigi.transform.position = new Vector2(babadomRigi.position.x, -1.49f - 0.03f * layer);
			if (speed.y < 0.0) speed.y = 0.0f;
			//ランダムで左右に速度がつく
			RndWalk();
		}
		//x座標はここまで0.0001f * Random.Range(-21500, 21500)
		if (Mathf.Abs(babadomRigi.position.x) > 2.1500f)
		{
			babadomRigi.transform.position = new Vector2(2.15f * Mathf.Sign(babadomRigi.position.x), babadomRigi.position.y);
			speed.x *= -1.0f;
		}
	}

	//吸引アルゴリズム
	void Move()
	{
		//もしマウスクリックされているなら
		if (UfoDust.MouseDownNow == 1)
		{
			Vector2 ufoPos;
			Vector2 posSub;
			float leng;
			float r1leng;
			float r2leng;
			ufoPos = UfoDust.ufoPos;//現在のufoの場所
			ufoPos.y -= 0.1f;//UFOの下の方に吸い込ませたい
			posSub = ufoPos - babadomRigi.position;//差分
			posSub.y *= 0.82f;//ここで距離の1.5乗に反比例して吸引力が働く。yの吸引力を強くしたいときは見かけ上のy差分を少なくすればいい
			leng = posSub.SqrMagnitude();//距離測定
			leng = Mathf.Max(0.00001f, leng);//0除算を避ける
			r1leng = 3.3f / leng;
			r2leng = 5.0f / (leng * leng);//1.5乗分の1
			speed += power * (ufoPos - babadomRigi.position) * (r1leng + r2leng);
			//スケール変換
			float scl = Mathf.Clamp(1.5f * leng + 0.24f, 0.4f, 1.0f);
			babadomRigi.transform.localScale = new Vector3(scl, scl, scl);
		}
		else
		{
			babadomRigi.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
		}
	}
}


//クオータニオンに関して
//http://marupeke296.com/DXG_No10_Quaternion.html