﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // ←※これを忘れずに入れる

public class UfoGage : MonoBehaviour
{
	int flm;
	static int HP;
	float gx;
	float gy;
	int width;
	int height;

	//	Vector2 ups;
	// 描画領域.
	Rect boxRect;
	Rect NormalRect;
	Rect NormalRect_fast;
	public Texture2D texture_HP;

	//プレハブ初期化
	static GameObject _prefabExpl;

	void Start()
	{
		flm = 0;
		HP = 0;
		width = Screen.width;
		height = Screen.height;
		NormalRect_fast = new Rect(0.0f, 0.0f, 1.0f, 1.0f);
	}

	void Update()
	{
		// 描画.
		if (flm < 120)
		{
			HP++;
		}
		flm++;
	}


	void OnGUI()
	{
		//下の白いやつ
		gx = (UfoMouseMove.GetUfoPos().x + 3.2f) * width / 6.4f;
		gy = (-UfoMouseMove.GetUfoPos().y - 0.5f + 2.4f) * height / 4.8f;
		boxRect = new Rect(gx - 60.0f * width / 640.0f, gy, 120.0f * width / 640.0f, 12.0f * height / 480.0f);
		GUI.color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
		GUI.DrawTextureWithTexCoords(boxRect, texture_HP, NormalRect_fast);
		//上にくるバー
		boxRect = new Rect(gx - 60.0f * width / 640.0f, gy, (float)(HP) * width / 640.0f, 12.0f * height / 480.0f);
		NormalRect = new Rect(0.0f, 0.0f, (float)(HP)/120.0f, 1.0f);
		GUI.color = new Vector4(0.0f, 0.9f, 0.1f, 1.0f);
		GUI.DrawTextureWithTexCoords(boxRect, texture_HP, NormalRect);
	}

	static public void DmgdHp(float v1)
	{
		HP -= (int)(100.0f * v1);
		//staticAddExpl(UfoMouseMove.GetUfoPos());

	}


	// 爆発エフェクトを生成
	static void staticAddExpl(Vector2 _rigi)
	{
		Instantiate(_prefabExpl, _rigi, Quaternion.identity);
		/*
		// プレハブを取得
		_prefab = Resources.Load("tama_big_explo") as GameObject;
		//インスタンス生成＝画面上に出す
		GameObject g = Instantiate(_prefab, _rigi.position, Quaternion.identity) as GameObject;
		//親子関係セット
		g.transform.parent = this.transform;
		return g;
		*/
	}


}