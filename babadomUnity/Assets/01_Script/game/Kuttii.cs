﻿using UnityEngine;
using System.Collections;

public class Kuttii : MonoBehaviour
{
	//弾発射スピード
	public float tamaSpeed;
	// プレハブ
	GameObject _prefab = null;
	//弾位置修正
	Vector3 tamaSetPos = new Vector3(-0.2f, -0.05f, 0.0f);

	void Start()
	{
		//_tamaSmall = new TamaSmall();

		//_gameobj = this.transform.Find("TamaTama").gameObject;
		//_tamaSmall = _gameobj.GetComponent<TamaSmall>();
		//Debug.Log(_tamaSmall.tmppp());
		//_tamaSmall.name = "tamaaaa";

	}

	// Update is called once per frame
	void Update()
	{
		//kuttiiのridibody使う
		Rigidbody2D kuttiiRigi = GetComponent<Rigidbody2D>();

		//クッチー星人が撃った弾が画面上に存在しているかどうか
		if (this.transform.childCount == 0)
		{

			if (Random.Range(0, Mathf.Clamp(1000 - 93 * GameManagerGod.level, 1, 999999)) == 0)
			{
				//_tamaSmall = Add(kuttiiRigi.position);
				Add(kuttiiRigi.position);
			}
		}
	}

	// 弾の生成,弾を子オブジェクトとして生成
	public TamaSmall Add(Vector3 xyz)
	{
		// プレハブを取得
		_prefab = Resources.Load("tama_small") as GameObject;
		// プレハブからインスタンスを生成

		//発生位置補正
		Vector3 tmp;
		if (tamaSpeed < 0.0)
		{
			tmp = xyz + tamaSetPos;
		}
		else
		{
			tmp = xyz + new Vector3(-tamaSetPos.x, tamaSetPos.y, tamaSetPos.z);
		}

		GameObject g = Instantiate(_prefab, tmp, Quaternion.identity) as GameObject;
		g.transform.parent = this.transform;
		TamaSmall obj = g.GetComponent<TamaSmall>();
		//高さごとにスピード違う
		obj.tamaSpeed = tamaSpeed;
		
		//画像反転
        if (tamaSpeed > 0.0)
		{
			obj.transform.localScale = new Vector3(-obj.transform.localScale.x, obj.transform.localScale.y, obj.transform.localScale.z);
        }

		return obj;
	}

	// 子オブジェクトの数を判断
	//http://qiita.com/hiroyuki7/items/95c66aee26115cf24a19
	//http://baba-s.hatenablog.com/entry/2014/04/17/171606
}
