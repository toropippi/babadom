﻿using UnityEngine;
using System.Collections;

public class UfoEffect : MonoBehaviour
{
	//エフェクトグラフィックの一番下y座標
	float EfxYpos = -2.0f;
	//現在のフレームでマウスが押されているか。結果的にGetMouseButtonの呼び出し回数を大幅に減らせ高速化できる
	public static int MouseDownNow = 0;
	//現在のフレームでマウスが押されているとき、Ufoの座標をstatic変数に入れる
	public static Vector2 ufoPos;
	//rigi
	Rigidbody2D EfxRigi;

	// Use this for initialization
	void Start()
	{
		//ufoEffectのridibody使う
		EfxRigi = GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void Update()
	{
		//もしマウスクリックされているなら
		if (UfoDust.MouseDownNow == 1)
		{
			float UfoPosy = UfoDust.ufoPos.y - 0.115f;
			float scl = UfoPosy - EfxYpos;
			scl *= 0.31f;
			EfxRigi.transform.localScale = new Vector3(scl, scl, scl);
			EfxRigi.transform.position = new Vector2(UfoDust.ufoPos.x, 0.5f * (UfoPosy + EfxYpos));
		}
		else
		{
			//見えなくスル
			EfxRigi.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
		}
	}
}
