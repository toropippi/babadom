﻿using UnityEngine;
using System.Collections;

public class TamaBig : MonoBehaviour
{
	//リジ
	Rigidbody2D oTmRigi;
	public float G;//重力加速度、セットは砲身スクリプトで
	public float spdx;//速度x、セットは砲身スクリプトで
	public float spdy;//速度y、セットは砲身スクリプトで

	//プレハブ初期化
	public GameObject _prefab;

	// Use this for initialization
	void Start()
	{
		//大玉のリギ取得
		oTmRigi = GetComponent<Rigidbody2D>();
		//重力加速度取得
		//G = 0.0308f / GameManagerGod.flmRate;
	}

	// Update is called once per frame
	void Update()
	{
		if (oTmRigi.position.y < -1.799)
		{
			Add(oTmRigi);
			Destroy(gameObject);
		}

		//移動
		Velocity(oTmRigi);
		//重力加速度計算
		Acceleration();
	}


	//自作移動ルーチン
	void Velocity(Rigidbody2D _rigi)
	{
		_rigi.transform.position = _rigi.position + new Vector2(spdx, spdy);
	}
	//自作重力加速度計算
	void Acceleration()
	{
		spdy += G;
	}

	// 泥爆発エフェクトを生成
	void Add(Rigidbody2D _rigi)
	{
		Instantiate(_prefab, _rigi.position + new Vector2(0.0f, 0.35f), Quaternion.identity);
		/*
		// プレハブを取得
		_prefab = Resources.Load("tama_big_explo") as GameObject;
		//インスタンス生成＝画面上に出す
		GameObject g = Instantiate(_prefab, _rigi.position, Quaternion.identity) as GameObject;
		//親子関係セット
		g.transform.parent = this.transform;
		return g;
		*/
	}


	//衝突判定
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			//Rigidbody2D 自分のrigi;
			Rigidbody2D my_rigi = this.GetComponent<Rigidbody2D>();
			Vector2 colPos = my_rigi.position;
			Rigidbody2D u_rigi = col.GetComponent<Rigidbody2D>();
			Vector2 ufoPos = u_rigi.position;
			//Vector2 ufoPos = UfoMouseMove.GetUfoPos();
			//長さ1ベクトル計算
			Vector2 fv = ufoPos - colPos;
			fv.Normalize();
			//ufoに外力加える
			UfoMouseMove.AddFUfo(fv, 4.6f);
			UfoMouseMove.AddFUfo(new Vector2(spdx, spdy), GameManagerGod.flmRate * (1.0f + 0.4f * UfoDust.MouseDownNow));
			//HP操作
			UfoGage.DmgdHp(0.16f);
			//自分は消滅
			Destroy(gameObject);
		}
	}

}