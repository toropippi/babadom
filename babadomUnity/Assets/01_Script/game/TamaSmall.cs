﻿using UnityEngine;
using System.Collections;

public class TamaSmall : MonoBehaviour
{
	// 弾の基礎スピード
	public float tamaSpeed;
	//スピード倍率//高さによって弾の速度が違う
	float spdRate;
	Rigidbody2D tamaSmallRigi;

	//Use this for initialization
	void Start()
	{
		//tamaSmallのridibody使う
		tamaSmallRigi = GetComponent<Rigidbody2D>();
		//オブジェクトの速度にセット
		//tama.velocity = new Vector2(tamaSpeed, 0.0f);
	}

	// Update is called once per frame
	void Update()
	{
		//もし画面外なら
		if (Mathf.Abs(tamaSmallRigi.position.x) > 3.3f)
		{
			Destroy(gameObject);
		}

		//移動
		Velocity(tamaSmallRigi);
	}


	//自作移動ルーチン
	void Velocity(Rigidbody2D _rigi)
	{
		_rigi.transform.position = _rigi.position + new Vector2(tamaSpeed / GameManagerGod.flmRate, 0.0f);
	}

	//衝突判定
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			//Rigidbody2D 自分のrigi;
			Rigidbody2D my_rigi = this.GetComponent<Rigidbody2D>();
			Vector2 colPos = my_rigi.position;
			Rigidbody2D u_rigi = col.GetComponent<Rigidbody2D>();
			Vector2 ufoPos = u_rigi.position;
			//Vector2 ufoPos = UfoMouseMove.GetUfoPos();
			//長さ1ベクトル計算
			Vector2 fv = ufoPos - colPos;
			fv.Normalize();
			//ufoに外力加える
			UfoMouseMove.AddFUfo(fv, 4.0f);
			//HP操作
			UfoGage.DmgdHp(0.05f);
			//自分は消滅
			Destroy(gameObject);
		}
	}

	/*
	//衝突時
	void OnCollisionEnter() {
		Destroy(gameObject);
		//if (other.gameObject.CompareTag('Player'))
		//{
		//	//move to this object to above.
		//	gameObject.transform.position = Vector3.up * 5;
		//}
	}
	*/
}

