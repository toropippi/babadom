﻿using UnityEngine;
using System.Collections;

public class BabadomParent : MonoBehaviour
{
	//画面に出る馬場ドンのmax量
	public int MaxBaba = 2200;
	// プレハブ
	GameObject _prefab = null;


	// Use this for initialization
	void Start()
	{
		for (int i = 0; i < MaxBaba; i++)
		{
			Add(Random.Range(0, 10000), 0);
		}
	}

	// Update is called once per frame
	void Update()
	{

		//馬場ドン聖人が何人イルカ
		if (this.transform.childCount < MaxBaba)
		{
			for (uint i = 0; i < (MaxBaba - this.transform.childCount) / 12 + 1; i++)
			{
				Add(Random.Range(0, 10000), 1);
			}
		}
	}

	// ババドンの生成,ババドンを子オブジェクトとして生成
	public Babadom Add(int layer, int mode)
	{
		// プレハブを取得;
		_prefab = Resources.Load("babadom_seijin") as GameObject;
		//発生位置;
		Vector3 xyz;
		float lr;//右左フラグ
		lr = 2.0f * Random.Range(0, 2) - 1.0f;
		//もし初期ババ丼なら
		if (mode == 0)
		{
			xyz = new Vector3(0.0001f * Random.Range(-21500, 21500), -1.49f, 0.0001f * layer);
		}
		else
		{
			xyz = new Vector3(2.15f * lr, -1.47f-0.03f* (layer / 2500), 0.0001f * layer);
		}
		//Unity obj生成
		GameObject g = Instantiate(_prefab, xyz, Quaternion.identity) as GameObject;
		g.transform.parent = this.transform;
		Babadom obj = g.GetComponent<Babadom>();
		//左右から発生したババドンなら
		if (mode == 1)
		{
			obj.speed.x = -0.01f * Random.Range(80, 399) * lr;
		}
		//ババドンのレイヤー、数字が大きいほど手前でかつ下にいることになる
		obj.layer = layer / 2500;
		return obj;
	}
}

