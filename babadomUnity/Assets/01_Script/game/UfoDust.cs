﻿using UnityEngine;
using System.Collections;

public class UfoDust : MonoBehaviour
{
	//現在のフレームでマウスが押されているか。結果的にGetMouseButtonの呼び出し回数を大幅に減らせ高速化できる
	public static int MouseDownNow = 0;
	//現在のフレームでマウスが押されているとき、Ufoの座標をstatic変数に入れる
	public static Vector2 ufoPos;
	//アニメーター
	Animator animator;
	//フレームをソースから指定したい
	double flame;

	// Use this for initialization
	void Start()
	{
		animator = GetComponent<Animator>();
		flame = 0.0;
	}

	// Update is called once per frame
	void Update()
	{
		//現在のフレームでマウスが押されているか。結果的にGetMouseButtonの呼び出し回数を大幅に減らせ高速化できる
		if (Input.GetMouseButton(0))
		{
			MouseDownNow = 1;
			ufoPos = UfoMouseMove.GetUfoPos();
			flame += 0.02;
			if (flame >= 0.60) { flame = 0.60; }
			animator.SetTime(flame);
		}
		else
		{
			flame -= 0.02;
			if (flame <= 0.02) { flame = 0.02; }
			animator.SetTime(flame);
			MouseDownNow = 0;
		}
	}

}
