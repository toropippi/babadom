﻿using UnityEngine;
using System.Collections;

public class BackBuilding : MonoBehaviour
{
	public float speed = 7.2f;
	public float reversePos = 1.8f;//
	int flg = 1;//1なら左に動く、-1なら右に動く

	void Update()
	{
		//backbildingのridibody使う
		Rigidbody2D backBuildingRigi = GetComponent<Rigidbody2D>();
		// 左へ移動
		// * Time.deltaTime
		transform.position += Vector3.left * speed * flg / GameManagerGod.flmRate;

		//もし端っこにいったら速度反転
		if (Mathf.Abs(backBuildingRigi.position.x) > reversePos)
		{
			flg *= -1;
		}

	}

	//objが画面外にでた時に実行される関数
	/*
    void OnBecameInvisible()
    {
        // スプライトの幅を取得
        float width = GetComponent<SpriteRenderer>().bounds.size.x;
        // 幅ｘ個数分だけ右へ移動
        transform.position += Vector3.right * width * spriteCount;
    }
    */
}