﻿using UnityEngine;
using System.Collections;

public static class GameManagerGod{

	//レベルを全クラスで共有したい
	public static int level = 5;
	//難易度を全クラスで共有したい
	public static int difficulty = 0;
	//基本フレームレートを全クラスで共有したい
	public static float flmRate = 60.0f;

	//条件式を整数で返す
	public static int rif(bool p1)
	{
		if (p1 == true)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	//条件式を浮動小数点で返す
	public static float riff(bool p1)
	{
		if (p1 == true)
		{
			return 1.0f;
		}
		else
		{
			return 0.0f;
		}
	}

	/*
	void Start () {
	}

	void Update () {
	}
	*/
}
